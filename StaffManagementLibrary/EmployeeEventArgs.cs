﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaffManagementLibrary
{
    public class EmployeeEventArgs : EventArgs
    {
        public Employee Employee { get; set; }
        public EmployeeEventArgs(Employee Employee)
        {
            this.Employee = Employee;
        }
    }
}
