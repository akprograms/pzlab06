﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaffManagementLibrary
{
    public class Company
    {
        public List<Employee> Employees { get; set; }

        public void GetItOnConsoleGodDammit()
        {
            foreach (var item in Employees)
            {
                Console.WriteLine(item);
            }
        }

        public void UseEmployeeMethod (EmployeeDelegate employeeDelegate)
        {
            foreach(Employee e in Employees)
            {
                employeeDelegate(e);
            }
        }

        public Employee FindKowalski()
        {
            return Employees.Find(e => e.Surname == "Kowalski");
        }

        public Employee FindEmployee(String Name, String Surname)
        {
            return Employees.Find(e => e.Name == Name && e.Surname == Surname);
        }

        public List<Employee> FindGoodSellers()
        {
            return Employees.FindAll(e => e.SellCount > 10);
        }

        public List<Employee> FindEmployeeByExp(int Experience)
        {
            return Employees.FindAll(e => e.Experience > Experience);
        }

        private EmployeeDelegate OnRemove;

        public void AddEmplDel(EmployeeDelegate del)
        {
            OnRemove += del;
        }

        public void RemoveEmplDel(EmployeeDelegate del)
        {
            OnRemove -= del;
        }

        public void DeleteEmployee1(int Index)
        {
            Employee Empl = Employees[Index];
            Employees.RemoveAt(Index);
            if (OnRemove != null)
            {
                OnRemove(Empl);
            }
        }

        public event EmployeeDelegate OnRemove2;

        public void DeleteEmployee2(int Index)
        {
            Employee Empl = Employees[Index];
            Employees.RemoveAt(Index);
            if (OnRemove2 != null)
            {
                OnRemove2(Empl);
            }
        }

        public event EventHandler<EmployeeEventArgs> OnRemove3;

        public void DeleteEmployee3(int Index)
        {
            Employee Empl = Employees[Index];
            Employees.RemoveAt(Index);
            if (OnRemove3 != null)
            {
                OnRemove3(this, new EmployeeEventArgs(Empl));
            }
        }

        public delegate String DisplayHandler(Employee element);

        public void DisplayInfo(DisplayHandler whatToDisplay)
        {
            foreach(Employee e in Employees)
            {
                Console.WriteLine(whatToDisplay(e));
            }
        }

        public delegate bool DisplayHandler2(Employee element);

        public List<Employee> FindEmployeeBy(DisplayHandler2 whatToDisplay)
        {
            List<Employee> lista = new List<Employee>();
            
            foreach (Employee e in Employees)
            {
                if(whatToDisplay(e))
                {
                    lista.Add(e);
                }
            }
            return lista;
        }

        //public delegate IComparable DisplayHandler3(Employee element);

        public void SortEmployeeBy(DisplayHandler whatToDisplay)
        {
            Employees.OrderBy(e => whatToDisplay(e));
        }
    }
}
