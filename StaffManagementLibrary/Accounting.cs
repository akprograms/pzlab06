﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaffManagementLibrary
{
    public static class Accounting
    {
        private static void showName(Employee employee)
        {
            Console.WriteLine(employee.Name + " " + employee.Surname);
        }

        public static void SallaryIncrease(Employee employee)
        {
            if(employee.SellCount > 10)
            {
                employee.Salary += 200;
                showName(employee);
            }
        }

        public static void SallaryDecrease(Employee employee)
        {
            if (employee.Experience < 2)
            {
                employee.Salary -= employee.Salary/10;
                showName(employee);
            }
        }

        public static void ZeroSellCount(Employee employee)
        {
            employee.SellCount = 0;
            showName(employee);
        }
    }
}
