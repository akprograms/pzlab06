﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaffManagementLibrary
{
    public delegate void EmployeeDelegate(Employee employee);

    public class Employee
    {
        public String Name { get; set; }
        public String Surname { get; set; }
        public int Experience { get; set; }
        public int SellCount { get; set; }
        public int Salary { get; set; }

        public Employee(String Name, String Surname, int Experience, int SellCount, int Salary)
        {
            this.Name = Name;
            this.Surname = Surname;
            this.Experience = Experience;
            this.SellCount = SellCount;
            this.Salary = Salary;
        }

        public override string ToString()
        {
            return string.Format("{0} {1}: {2}y, {3} sells, {4}$", Name, Surname, Experience, SellCount, Salary);
        }

    }
}
