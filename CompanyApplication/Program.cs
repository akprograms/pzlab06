﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StaffManagementLibrary;

namespace CompanyApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Company company = new Company();

            Employee e1 = new Employee("Jan", "Kowalski", 3, 11, 1500);
            Employee e2 = new Employee("John", "Doe", 2, 15, 2100);
            Employee e3 = new Employee("Adam", "Nowak", 4, 50, 4200);
            Employee e4 = new Employee("Zbigniew", "Wodecki", 1, 5, 1100);

            List<Employee> empl = new List<Employee>{ e1, e2, e3, e4 };

            company.Employees = empl;

            company.GetItOnConsoleGodDammit();

            EmployeeDelegate ed1 = new EmployeeDelegate(Accounting.SallaryIncrease);
            Console.WriteLine();
            ed1(e1);
            Console.WriteLine();
            Console.WriteLine(company.FindKowalski().ToString());
            Console.WriteLine();
            Console.WriteLine(company.FindEmployee("Adam","Nowak").ToString());
            Console.WriteLine();
            foreach (var item in company.FindGoodSellers())
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
            foreach (var item in company.FindEmployeeByExp(2))
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
            company.DisplayInfo(e => e.Name);
            Console.WriteLine();
            foreach (var item in company.FindEmployeeBy(e => e.Salary > 2000))
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
            company.SortEmployeeBy(e => e.Surname);
            foreach (var item in company.Employees)
            {
                Console.WriteLine(item);
            }
        }
    }
}
