﻿namespace Lab06
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxA1 = new System.Windows.Forms.TextBox();
            this.textBoxB1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.wynik1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.textBoxA2 = new System.Windows.Forms.TextBox();
            this.textBoxB2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.wynik2 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.textBoxA3 = new System.Windows.Forms.TextBox();
            this.textBoxB3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.wynik3 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.textBoxA4 = new System.Windows.Forms.TextBox();
            this.textBoxB4 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.wynik4 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.textBoxA5 = new System.Windows.Forms.TextBox();
            this.textBoxB5 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.wynik5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(149, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "=";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.MouseEnter += new System.EventHandler(this.button1_Enter);
            // 
            // textBoxA1
            // 
            this.textBoxA1.Location = new System.Drawing.Point(12, 26);
            this.textBoxA1.Name = "textBoxA1";
            this.textBoxA1.Size = new System.Drawing.Size(53, 20);
            this.textBoxA1.TabIndex = 1;
            // 
            // textBoxB1
            // 
            this.textBoxB1.Location = new System.Drawing.Point(90, 25);
            this.textBoxB1.Name = "textBoxB1";
            this.textBoxB1.Size = new System.Drawing.Size(53, 20);
            this.textBoxB1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "+";
            // 
            // wynik1
            // 
            this.wynik1.AutoSize = true;
            this.wynik1.Location = new System.Drawing.Point(224, 29);
            this.wynik1.Name = "wynik1";
            this.wynik1.Size = new System.Drawing.Size(34, 13);
            this.wynik1.TabIndex = 4;
            this.wynik1.Text = "wynik";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(149, 60);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(56, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "=";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // textBoxA2
            // 
            this.textBoxA2.Location = new System.Drawing.Point(12, 62);
            this.textBoxA2.Name = "textBoxA2";
            this.textBoxA2.Size = new System.Drawing.Size(53, 20);
            this.textBoxA2.TabIndex = 6;
            // 
            // textBoxB2
            // 
            this.textBoxB2.Location = new System.Drawing.Point(90, 61);
            this.textBoxB2.Name = "textBoxB2";
            this.textBoxB2.Size = new System.Drawing.Size(53, 20);
            this.textBoxB2.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(71, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "-";
            // 
            // wynik2
            // 
            this.wynik2.AutoSize = true;
            this.wynik2.Location = new System.Drawing.Point(224, 65);
            this.wynik2.Name = "wynik2";
            this.wynik2.Size = new System.Drawing.Size(34, 13);
            this.wynik2.TabIndex = 9;
            this.wynik2.Text = "wynik";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(149, 96);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(56, 23);
            this.button3.TabIndex = 10;
            this.button3.Text = "=";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBoxA3
            // 
            this.textBoxA3.Location = new System.Drawing.Point(12, 98);
            this.textBoxA3.Name = "textBoxA3";
            this.textBoxA3.Size = new System.Drawing.Size(53, 20);
            this.textBoxA3.TabIndex = 11;
            // 
            // textBoxB3
            // 
            this.textBoxB3.Location = new System.Drawing.Point(90, 97);
            this.textBoxB3.Name = "textBoxB3";
            this.textBoxB3.Size = new System.Drawing.Size(53, 20);
            this.textBoxB3.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(71, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "*";
            // 
            // wynik3
            // 
            this.wynik3.AutoSize = true;
            this.wynik3.Location = new System.Drawing.Point(224, 101);
            this.wynik3.Name = "wynik3";
            this.wynik3.Size = new System.Drawing.Size(34, 13);
            this.wynik3.TabIndex = 14;
            this.wynik3.Text = "wynik";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(149, 132);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(56, 23);
            this.button4.TabIndex = 15;
            this.button4.Text = "=";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // textBoxA4
            // 
            this.textBoxA4.Location = new System.Drawing.Point(12, 134);
            this.textBoxA4.Name = "textBoxA4";
            this.textBoxA4.Size = new System.Drawing.Size(53, 20);
            this.textBoxA4.TabIndex = 16;
            // 
            // textBoxB4
            // 
            this.textBoxB4.Location = new System.Drawing.Point(90, 133);
            this.textBoxB4.Name = "textBoxB4";
            this.textBoxB4.Size = new System.Drawing.Size(53, 20);
            this.textBoxB4.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(71, 137);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "/";
            // 
            // wynik4
            // 
            this.wynik4.AutoSize = true;
            this.wynik4.Location = new System.Drawing.Point(224, 137);
            this.wynik4.Name = "wynik4";
            this.wynik4.Size = new System.Drawing.Size(34, 13);
            this.wynik4.TabIndex = 19;
            this.wynik4.Text = "wynik";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(149, 167);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(56, 23);
            this.button5.TabIndex = 20;
            this.button5.Text = "=";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // textBoxA5
            // 
            this.textBoxA5.Location = new System.Drawing.Point(12, 169);
            this.textBoxA5.Name = "textBoxA5";
            this.textBoxA5.Size = new System.Drawing.Size(53, 20);
            this.textBoxA5.TabIndex = 21;
            // 
            // textBoxB5
            // 
            this.textBoxB5.Location = new System.Drawing.Point(90, 168);
            this.textBoxB5.Name = "textBoxB5";
            this.textBoxB5.Size = new System.Drawing.Size(53, 20);
            this.textBoxB5.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(71, 172);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "%";
            // 
            // wynik5
            // 
            this.wynik5.AutoSize = true;
            this.wynik5.Location = new System.Drawing.Point(224, 172);
            this.wynik5.Name = "wynik5";
            this.wynik5.Size = new System.Drawing.Size(34, 13);
            this.wynik5.TabIndex = 24;
            this.wynik5.Text = "wynik";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 209);
            this.Controls.Add(this.wynik5);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxB5);
            this.Controls.Add(this.textBoxA5);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.wynik4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxB4);
            this.Controls.Add(this.textBoxA4);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.wynik3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxB3);
            this.Controls.Add(this.textBoxA3);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.wynik2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxB2);
            this.Controls.Add(this.textBoxA2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.wynik1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxB1);
            this.Controls.Add(this.textBoxA1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxA1;
        private System.Windows.Forms.TextBox textBoxB1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label wynik1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBoxA2;
        private System.Windows.Forms.TextBox textBoxB2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label wynik2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBoxA3;
        private System.Windows.Forms.TextBox textBoxB3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label wynik3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBoxA4;
        private System.Windows.Forms.TextBox textBoxB4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label wynik4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox textBoxA5;
        private System.Windows.Forms.TextBox textBoxB5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label wynik5;
    }
}

