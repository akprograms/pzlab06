﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab06
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            button2.Click += new System.EventHandler(button2_Click);
            button3.Click += new System.EventHandler(button3_Click);
            button4.Click += delegate (object sender, EventArgs e)
            {
                wynik4.Text = (Int32.Parse(textBoxA4.Text) / Int32.Parse(textBoxB4.Text)).ToString();
            };
            button5.Click += (sender, e) => wynik5.Text = (Int32.Parse(textBoxA5.Text) % Int32.Parse(textBoxB5.Text)).ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            wynik1.Text = (Int32.Parse(textBoxA1.Text) + Int32.Parse(textBoxB1.Text)).ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            wynik2.Text = (Int32.Parse(textBoxA2.Text) - Int32.Parse(textBoxB2.Text)).ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            wynik3.Text = (Int32.Parse(textBoxA3.Text) * Int32.Parse(textBoxB3.Text)).ToString();
        }

        private void button1_Enter(object sender, EventArgs e)
        {
            wynik4.Text = "AAAAAA";
        }
    }
}
